package com.yz.mangosoft.fileservice.controller;

import com.yz.mangosoft.fileservice.model.DataSet;
import com.yz.mangosoft.fileservice.repository.FileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.util.Date;

@RestController
@RequestMapping(value = "/files")
public class FileServiceController {

    private final FileRepository fileRepository;

    @Autowired
    public FileServiceController(FileRepository fileRepository) {

        this.fileRepository = fileRepository;
    }

    @PostMapping("/upload")
    public ResponseEntity<Long> serveInstance(@RequestParam("file") MultipartFile file) {
        //deleteOldFile();
        DataSet dataSet = null;
        try {
            dataSet = new DataSet(file.getBytes(),new Date(),file.getOriginalFilename());
            fileRepository.save(dataSet);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(dataSet.getId(), HttpStatus.OK);

    }

    @PutMapping("/update/{id}")
    public void updateEntityById(@PathVariable Long id, @RequestParam("description") String description) {
        DataSet dataSet = fileRepository.findOne(id);
        dataSet.setDescription(description);
        fileRepository.save(dataSet);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteEntityById(@PathVariable Long id) {
        fileRepository.delete(id);
    }
}

