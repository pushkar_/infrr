package com.yz.mangosoft.fileservice.repository;

import com.yz.mangosoft.fileservice.model.DataSet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileRepository extends JpaRepository<DataSet, Long> {

}
