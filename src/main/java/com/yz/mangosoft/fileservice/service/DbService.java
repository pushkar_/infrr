package com.yz.mangosoft.fileservice.service;

import com.yz.mangosoft.fileservice.model.DataSet;
import com.yz.mangosoft.fileservice.repository.FileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;


@Component
public class DbService {

    private final FileRepository fileRepository;

    @Autowired
    public DbService(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }


    @Async
    @Scheduled(cron = "0 0 4 * * ?")
    public void deleteOldFile() {
        List<DataSet> dataSet = fileRepository.findAll();
        for (int i = 0; i <dataSet.size() ; i++) {
            Date date = new Date(System.currentTimeMillis() - 518400000l);

            if (date.after(dataSet.get(i).getDate())){
                fileRepository.delete(dataSet.get(i).getId());
            }
        }
    }
}
