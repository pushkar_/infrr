package com.yz.mangosoft.fileservice.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@javax.persistence.Entity
@Table(name = "files")
public class DataSet implements Serializable {

    /**
     * File.
     */
    @Column(name="file")
    private byte[] file;

    /**
     * File description.
     */
    @Column(name = "description")
    private String description;

    /**
     * File date.
     */
    @Column(name = "date")
    private Date date;

    /**
     * File name.
     */
    @Column(name = "name")
    private String name;

    /**
     * File id in DB.
     */
    @Id
    @GeneratedValue
    private long id;

    public DataSet(byte[] file, String description, Date date, String name) {
        this.file = file;
        this.description = description;
        this.date = date;
        this.name = name;
    }

    public DataSet() { }

    public DataSet(byte[] file, Date date, String name) {
        this.file = file;
        this.date = date;
        this.name = name;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) { this.file = file; }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() { return date; }

    public void setDate(Date date) { this.date = date; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }
}
