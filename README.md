infrr REST API project
===============

Functions
---------
 - user save the file on the server
 - adds a description to the file
 - deletes the file
 - gets the access key to the downloaded file
 - access to the file is possible by the key
    
Stack
---------
 Maven, Spring boot, Hibernate, Spring Bind, Postgres

REST API
---------
**Add a file**   
/files/upload?file={file} POST   
file: A file posted in a multipart request


**Add file description**   
/files/update/{id}?description={description} PUT   
id: file UUID in DB
description: file description

**Delete file**   
/files/delete/{id} DELETE   
id: file UUID in DB

REST Service Controller
---------
FileServiceController.java
Executes incoming request and defines URL to service method mappings. 
All remote call are delegated to the file repository.

Repository
---------
Interface: JpaRepository.java
Implementation: FileRepository.java
Object to insert, update and delete file. FileRepository saves files in the DB.
Each files in the DB has a Universally Unique Identifier (UUID) and creation time.


